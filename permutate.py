#!/usr/bin/env python3
import datetime
import random
import re
import sys

import pywikibot

if datetime.datetime.now() < datetime.datetime(2020, 7, 6, 10):
    print("Umfrage hat noch nicht begonnen")
    sys.exit()
if datetime.datetime.now() > datetime.datetime(2020, 7, 19):
    print("Umfrage beendet")
    sys.exit()

site = pywikibot.Site("de", "wikipedia")
page = pywikibot.Page(
    site, "Wikipedia:Umfragen/Technische Wünsche 2020 Themenschwerpunkte"
)
text = page.text


i = re.search(r"^\{\{/", text, re.M).span()[0]
for match in re.finditer(r"^\{\{/.*", text, re.M):
    pass
j = match.span()[1]

header = text[0:i]  # noqa: 203
footer = text[j:]  # noqa: 203
themen = text[i:j].split("\n")

random.shuffle(themen)

text = header + "\n".join(themen) + footer

page.text = text
page.save("Reihenfolge der Themenschwerpunkte permutiert")
