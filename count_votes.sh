#!/bin/bash

apiurl="https://de.wikipedia.org/w/api.php?"
basepage_withoutprefix="Umfragen/Technische Wünsche 2020 Themenschwerpunkte"
basepage="Wikipedia:Umfragen/Technische Wünsche 2020 Themenschwerpunkte"
resultsdir="results/"
filename="${resultsdir}2020.txt"
timestamp="$(date -Iminutes)"

cd $(dirname $0)

function get_page_content() {
	curl -s "${apiurl}" \
		--data-urlencode "action=query" \
		--data-urlencode "format=json" \
		--data-urlencode "prop=revisions" \
		--data-urlencode "rvprop=content" \
		--data-urlencode "rvlimit=1" \
		--data-urlencode "titles=${1}" \
	       | jq -r '.query .pages | .[] .revisions | .[0] ."*"'
}

mapfile -t pages < <(curl -s "${apiurl}" \
        --data-urlencode "generator=allpages" \
        --data-urlencode "gapnamespace=4" \
        --data-urlencode "gaplimit=max" \
	--data-urlencode "action=query" \
	--data-urlencode "format=json" \
	--data-urlencode "gapprefix=${basepage_withoutprefix}" \
	| jq -r '.query .pages | .[] .title')

nl="
"

totalusers=""
declare -A results
for page in "${pages[@]}"; do
	area=${page#$basepage}
	area=${area#/}
	[[ "$area" == "" ]] && continue
	[[ "$area" == "Vorlage Themenschwerpunkt" ]] && continue
	votes=$(get_page_content "$page" | \
		grep -oi '\(Benutzer\|Benutzerin\|User\):[^]|]*' | sort | uniq )
	if [[ "$votes" == "" ]]; then
		results[$area]=0
	else
		results[$area]=$(echo -e "$votes" | wc -l)
	fi
	totalusers="$totalusers$nl$votes"
done

for area in "${!results[@]}"; do
	echo -e "${results[$area]}\t$area"
	echo -e "${timestamp}\t${results[$area]}" >> "${resultsdir}$area.txt"
done | sort -nr | tee $filename

echo -en "Total number of votes: " | tee -a $filename
echo -e "$totalusers" | grep -v '^$' | wc -l | tee -a $filename

echo -en "Total number of voters: " | tee -a $filename
echo -e "$totalusers" | grep -v '^$' | sort | uniq | tee "${resultsdir}Abstimmende.txt" | wc -l | tee -a $filename

echo >> $filename
date >> $filename

gnuplot plot.plt
