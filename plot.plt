set datafile separator '\t'

set xdata time
set timefmt '%Y-%m-%dT%H:%M+00:00'

set xrange ["2020-07-06T10:02+00:00":]
set format x "%d.\n%H:00"


set terminal svg size 800,800  dynamic enhanced mouse
set output 'results/plot.svg'

set key left top
plot 'results/Hilfestellung beim Bearbeiten von Artikeln.txt' u 1:2 w lines t 'Hilfestellung beim Bearbeiten von Artikeln', \
	'results/Bessere Unterstützung von Geoinformationen.txt' u 1:2 w lines t 'Bessere Unterstützung von Geoinformationen', \
	'results/Bessere Unterstützung von Grafik, Audio, Video & Co.txt' u 1:2 w lines t 'Bessere Unterstützung von Grafik, Audio, Video & Co', \
	'results/Einfach einheitliche Typografie in Artikeln erzeugen.txt' u 1:2 w lines t 'Einfach einheitliche Typografie in Artikeln erzeugen', \
	'results/Fehler bei der Arbeit mit Belegen reduzieren.txt' u 1:2 w lines t 'Fehler bei der Arbeit mit Belegen reduzieren', \
	'results/Tools leichter entwickeln und finden.txt' u 1:2 w lines t 'Tools leichter entwickeln und finden', \
	'results/Vandalismus und destruktive Handlungen besser bekämpfen.txt' u 1:2 w lines t 'Vandalismus und destruktive Handlungen besser bekämpfen', \
	'results/Von Inhaltsänderungen erfahren, die mich interessieren.txt' u 1:2 w lines t 'Von Inhaltsänderungen erfahren, die mich interessieren', \
	'results/Zusammenarbeit on-Wiki leichter machen.txt' u 1:2 w lines t 'Zusammenarbeit on-Wiki leichter machen'



