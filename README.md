Utilities for conducting WMDE Technical Wishes annual survey 2020

# Overview
The survey is held on-wiki, and will be adapted for each year. 

Tools include,
* `count-votes.py` - Count votes and output into results/.
* `permutate.py` - Scramble the topic choices by editing a wiki list.

All jobs will be run periodically during the survey, and the results inspected manually.

# Configuration
Any scripts which need wiki write access (currently only `permutate.py`) will require the operator to create a bot password.  This can be done self-serve by visiting the [Special:BotPasswords](https://de.wikipedia.org/wiki/Spezial:BotPasswords) page, and creating a new bot with a name of your choosing.  Give it the "Edit existing pages" permission grant.  Once the bot is created you'll receive a generated password in a format looking like this,

> The new password to log in with USERNAME@BOTNAME is BOTPASSWORD

Copy these values into the files below,

```bash
# Edit user-config.py:
    usernames["wikipedia"]["de"] = "USERNAME"

# Create and edit user-password.py:
cp user-password.py.example user-password.py

    ('USERNAME', BotPassword('BOTNAME', 'BOTPASSWORD'))
```
